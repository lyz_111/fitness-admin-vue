import request from '@/utils/request'
export default {
  getAllPageSchedule(currentPage,pageSize){
    return request({
      url:`/schedule/${currentPage}/${pageSize}`,
      method:"get"
    })
  },
  getScheduleById(id){
    return request({
      url:`/schedule/get/${id}`,
      method:"get"
    })
  },
  getScheduleByCoachName(currentPage,pageSize,coachName){
    return request({
      url:`/schedule/select/${currentPage}/${pageSize}/${coachName}`,
      method:"get"
    })
  },
  deleteScheduleById(id){
    return request({
      url:`/schedule/${id}`,
      method:"delete"
    })
  },
  addOrUpdateSchedule(data){
    return request({
      url:"/schedule/update",
      method:"post",
      data
    })
  }
}
