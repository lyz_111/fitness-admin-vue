import request from '@/utils/request'

export default {
  // 单个删除
  removeById(id) {
    return request({
      url: `/gymDetail/${id}`,
      method: 'delete'
    })
  },
  // 添加和修改
  addOrUpdateGymPlace(data) {
    return request({
      url: '/gymDetail/modify',
      method: 'post',
      data
    })
  },
  // 编辑页面数据显示
  findGymByGymId(id) {
    return request({
      url: `/gymDetail/${id}`,
      method: 'get'
    })
  },
  findGymPlaceById(id) {
    return request({
      url: `/gymDetail/show/${id}`,
      method: 'get'
    })
  }
}
