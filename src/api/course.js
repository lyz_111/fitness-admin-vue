import request from '@/utils/request';

export default {
  //分页查询课程、根据课程名称及教练名称模糊查寻课程
  getCoursePage(currentPage, pageSize, data) {
    return request({
      url: `/course/page/${currentPage}/${pageSize}`,
      method: 'post',
      data
    })
  },
  //添加、修改
  saveOrUpdateCourse(data) {
    return request({
      url: '/course',
      method: 'post',
      data
    })
  },
  //根据id删
  deleteCourseById(courseId) {
    return request({
      url: `/course/${courseId}`,
      method: 'delete'
    })
  },
  //根据id查
  getCourseById(courseId) {
    return request({
      url: `/course/${courseId}`,
      method: 'get'
    })
  },
  getAll(){
    return request({
      url:"/course/all",
      method:"get"
    })
  }
}
