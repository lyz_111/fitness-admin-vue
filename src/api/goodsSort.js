import request from '@/utils/request'

export default {
  queryGoodsSortApi(pageNum,pageSize,goodsSortDTO){
    return request({
      method: 'post',
      url: `/goodsSort/${pageNum}/${pageSize}`,
      data: goodsSortDTO
    })
  },
  addGoodsSort(goodsSortVo){
    return request({
      method: 'post',
      url: '/goodsSort',
      data: goodsSortVo
    })
  },
  updateGoodsSort(goodsSortVo){
    return request({
      method: 'put',
      url: '/goodsSort',
      data: goodsSortVo
    })
  },
  queryGoodsSortId(id){
    return request({
      method: 'get',
      url: `/goodsSort/${id}`
    })
  },
  deleteGoodsSortApi(id){
    return request({
      method: 'delete',
      url: `/goodsSort/${id}`
    })
  },
  deleteAllGoodsSort(ids){
      return request({
        method: 'delete',
        url: `/goodsSort/${ids}`
      })
  }
}
