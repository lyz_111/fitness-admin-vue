import request from '@/utils/request'

export function login(data) {
  return request({
    // 请求的地址
    url: '/user/login',
    // 请求的方式
    method: 'post',
    // 请求的数据 post请求发送数据时使用data  get请求发送数据时使用 params
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: {token}
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}


export default {
  //分页查询课用户
  getAllPage(currentPage, pageSize, data) {
    return request({
      url: `/user/${currentPage}/${pageSize}`,
      method: 'post',
      data
    })
  },
  deltById(id) {
    return request({
      url: `/user/delete/${id}`,
      method: 'delete'
    })
  },
  deltAll(data) {
    return request({
      url: `/user/delete`,
      method: 'delete',
      data
    })
  },
  getById(id) {
    return request({
      url: `/user/${id}`,
      method: 'get'
    })
  },
  getAddressByUserId(id) {
    return request({
      url: `/user/address/${id}`,
      method: 'get'
    })
  },
  addOrUpdata(data){
    return request({
      url:`/user/addOrUpdata`,
      method:'post',
      data
    })
  },
  getCurrentUserId(){
    return request({
      url:"/user/getuserid",
      method:"get"
    })
  }
}
