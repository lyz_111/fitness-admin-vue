import request from '@/utils/request';

export default {
  //分页查询课程、根据课程名称及教练名称模糊查寻课程
  getCommentAll(currentPage, pageSize) {
    return request({
      url: `/comment/${currentPage}/${pageSize}`,
      method: 'get',
    })
  },
  getSelectComment(currentPage, pageSize,category) {
    return request({
      url: `/comment/${currentPage}/${pageSize}`,
      method: 'post',
      params:{
        category
      }
    })
  },
  deltById(id) {
    return request({
      url:`/comment/${id}`,
      method:'delete'
    })
  }
}
