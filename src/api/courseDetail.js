import request from '@/utils/request';

export default {
  //根据courseId查询课程细节
  getCourseDetailByCourseId(courseId) {
    return request({
      url: `/courseDetail/${courseId}`,
      method: 'get'
    })
  },
  //添加、修改课程描述（返回courseId）
  saveOrUpdateCourseDetail(data) {
    return request({
      url: '/courseDetail',
      method: 'post',
      data
    })
  },

}
