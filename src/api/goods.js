import request from '@/utils/request'

export default {
  queryGoodsPage(pageNum,pageSize,goodsDTO){
      return request({
        method: 'post',
        url: `/goods/${pageNum}/${pageSize}`,
        data: goodsDTO
      })
  },
  deleteGoodsId(id){
      return request({
        method: 'delete',
        url: `/goods/${id}`
      })
  },
  deleteAllGoods(ids){
    return request({
      method: 'delete',
      url: `/goods`,
      data: ids
    })
  },
  addGoods(goodsAddDTO){
    return request({
      method: 'post',
      url: '/goods',
      data: goodsAddDTO
    })
  },
  updateGoods(goodsUpdateDTO){
    return request({
      method: 'put',
      url: '/goods',
      data: goodsUpdateDTO
    })
  },
  queryGoodsId(id){
    return request({
      method: 'get',
      url: `/goods/${id}`
    })
  },
  queryOnline() {
    return request({
      method: 'get',
      url: `/goods/onLine`,
    });
  },
  queryNoOnline() {
    return request({
      method: 'get',
      url: `/goods/no/onLine`,
    });
  },
}
