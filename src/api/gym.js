import request from '@/utils/request'

export default {
  // 分页+模糊查询
  listSearch(currentPage, pageSize, data) {
    return request({
      url: `/gym/${currentPage}/${pageSize}`,
      method: 'post',
      data
    })
  },
  // 单个删除
  removeById(id) {
    return request({
      url: `/gym/${id}`,
      method: 'delete'
    })
  },
  // 添加和修改
  addOrUpdateGym(data) {
    return request({
      url: '/gym',
      method: 'post',
      data
    })
  },
  // 批量删除
  deleteBatch(data) {
    return request({
      url: '/gym/delBatch',
      method: 'delete',
      data
    })
  },
  // 编辑页面数据显示
  findGymById(id) {
    return request({
      url: `/gym/${id}`,
      method: 'get'
    })
  },
  // 查询所有
  findAllGym() {
    return request({
      url: '/gym/all',
      method: 'get'
    })
  },
  //查询正常使用的场馆
  getGymByStatus(){
    return request({
      url: '/gym/byStatus',
      method: 'get'
    })
  },
}
