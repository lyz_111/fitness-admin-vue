import request from '@/utils/request';

export default {
  //分页查询课程、根据课程名称及教练名称模糊查寻课程
  getCoachPage(currentPage, pageSize, data) {
    return request({
      url: `/coach/${currentPage}/${pageSize}`,
      method: 'post',
      data
    })
  },
  //添加、修改
  saveOrUpdateCoach(data) {
    return request({
      url: '/coach',
      method: 'post',
      data
    })
  },
  //根据id删
  deleteCoachById(courseId) {
    return request({
      url: `/coach/${courseId}`,
      method: 'delete'
    })
  },
  //根据id查
  getCoachById(courseId) {
    return request({
      url: `/coach/${courseId}`,
      method: 'get'
    })
  },
  //根据id查
  getCoachAll() {
    return request({
      url: `/coach/all`,
      method: 'get'
    })
  },
  //全场馆查询
  getGymAll() {
    return request({
      url: `/gym/all`,
      method: 'get'
    })
  },
  //根据场馆id查询教练
  getCoachByGymId(gymId){
    return request({
      url: `/coach/gym/${gymId}`,
      method: 'get'
    })
  },
  //查询正常启用教练
  getCoachByOnline(){
    return request({
      url: '/coach/byOnline',
      method: 'get'
    })
  },
  //查询正常场馆下正常启用教练
  getCoach(){
    return request({
      url: '/coach/normal',
      method: 'get'
    })
  },
}
